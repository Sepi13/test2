#include "jeu.h"
#include <ctime>
#include "robot.h"
#include "joueur.h"
#include <vector>
#include "graphics.h"
#include <windows.h>


jeu::jeu(int nbRobot,int nbRobotAmel, terrain &t, int tailleAffichage): d_joueur{t.x()/2 , t.y()/2 , t , JOUEUR}, robots{nbRobot, nbRobotAmel,t},d_t{t} ,d_tailleAffiche{tailleAffichage}
{
    d_joueur.placeTerrain();
}

jeu::jeu(terrain &t, int tailleAffichage): d_joueur{t.x()/2 , t.y()/2 , t , JOUEUR}, robots{t},d_t{t} ,d_tailleAffiche{tailleAffichage}
{
    d_joueur.placeTerrain();
}


jeu::~jeu()
{}

bool jeu::lancer()
{
    afficherTerrain();
    bool fin=false;
    while(!fin)
    {
        int resultat=getch();
        bool aAvance=d_joueur.avance(resultat);
        while(!aAvance)
        {
            int resultat=getch();
            aAvance=d_joueur.avance(resultat);
        }
        robots.avancerRobots(d_joueur);
        fin = robots.testerCollision(d_joueur);
        cleardevice();
        afficherTerrain();
    }
   /* finJeu();*/
    int k=getch();
    cleardevice();
    return k=='r';
}

void jeu::afficherTerrain()
{
    for(int i=0;i<d_t.x(); i++)
    {
        for(int j=0; j<d_t.y(); j++)
        {
            int pointGauche = i*(d_tailleAffiche/d_t.x());
            int pointDroit = i*(d_tailleAffiche/d_t.x())+(d_tailleAffiche/d_t.x());
            int pointHaut = j*(d_tailleAffiche/d_t.y());
            int pointBas = j*(d_tailleAffiche/d_t.y())+(d_tailleAffiche/d_t.y());
            setcolor(BLACK);
            rectangle(pointGauche,pointHaut,pointDroit,pointBas);
            setcolor(WHITE);
            if(d_t.renvoiElement(i, j)==ROBOT)
            {
                setcolor(RED);
                bar(pointGauche,pointHaut, pointDroit, pointBas);
                setcolor(WHITE);
            }
            else if(d_t.renvoiElement(i, j)==JOUEUR)
            {
                bar(pointGauche,pointHaut, pointDroit, pointBas);
            }
            else if(d_t.renvoiElement(i, j)==ROBOTAMELIORE)
            {
                setcolor(BLUE);
                bar(pointGauche,pointHaut, pointDroit, pointBas);
                setcolor(WHITE);
            }
            else if(d_t.renvoiElement(i, j)==DEBRIS)
            {
                setcolor(YELLOW);
                bar(pointGauche,pointHaut, pointDroit, pointBas);
                setcolor(WHITE);
            }
            else if(d_t.renvoiElement(i, j)!=0)
            {
                setcolor(GREEN);
                bar(pointGauche,pointHaut, pointDroit, pointBas);
                setcolor(WHITE);
            }
        }
    }
}

void jeu::finJeu()
{
    for(int j=0;j<d_t.x(); j++)
    {
        for(int i=0; i<d_t.y(); i++)
        {
            int pointGauche = i*(d_tailleAffiche/d_t.x());
            int pointDroit = i*(d_tailleAffiche/d_t.x())+(d_tailleAffiche/d_t.x());
            int pointHaut = j*(d_tailleAffiche/d_t.y());
            int pointBas = j*(d_tailleAffiche/d_t.y())+(d_tailleAffiche/d_t.y());
            setcolor(BLACK);
            rectangle(pointGauche,pointHaut,pointDroit,pointBas);
            setcolor(WHITE);
            setcolor(RED);
            bar(pointGauche,pointHaut, pointDroit, pointBas);
            Sleep(1);
        }
    }
}
