#include "joueur.h"
#include "terrain.h"

/**Construis un joueur grace au constructeur "personne" */
joueur::joueur(int x, int y, terrain&t, int element): personne{x, y, t, element}
{}

/** Fais avancer joueur dans la direction "resultat"
    return vrai si le joueur a appuyer sur  faux sinon
*/
bool joueur::avance(const char&resultat)
{
        /**Si resultat=s on appel deplaceBas qui deplace d'une case vers le bas*/
        if(resultat=='s')
        {
            deplaceBas();
            return true;
        }
         /**Si resultat=z on appel deplaceHaut qui deplace d'une case vers le haut*/
        else if(resultat=='z')
        {
            deplaceHaut();
            return true;
        }
         /**Si resultat=q on appel deplaceGauche qui deplace d'une case vers la gauche*/
        else if(resultat=='q')
        {
            deplaceGauche();
            return true;
        }
         /**Si resultat=d on appel deplaceDroite qui deplace d'une case vers la droite*/
        else if(resultat=='d')
        {
            deplaceDroite();
            return true;
        }
         /**Si resultat=a on appel deplaceHaut et deplaceGauche qui deplace d'une case vers le haut, puis vers la gauche*/
        else if(resultat=='a')
        {
            deplaceHaut();
            deplaceGauche();
            return true;
        }
         /**Si resultat=w on appel deplaceBas et deplaceGauche qui deplace d'une case vers le bas, puis vers la gauche*/
        else if(resultat=='w')
        {
            deplaceBas();
            deplaceGauche();
            return true;
        }
         /**Si resultat=x on appel deplaceBas et deplaceDroite qui deplace d'une case vers le bas, puis vers la droite*/
        else if(resultat=='x')
        {
            deplaceBas();
            deplaceDroite();
            return true;
        }
         /**Si resultat=e on appel deplaceHaut et deplaceDroite qui deplace d'une case vers le haut, puis � droite */
        else if(resultat=='e')
        {
            deplaceHaut();
            deplaceDroite();
            return true;
        }
        return false;
}
