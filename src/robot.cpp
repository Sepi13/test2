#include "robot.h"
#include "joueur.h"
#include "terrain.h"
robot::robot(int x, int y, terrain&t, int element):personne {x, y, t, element}
{}

/**
 * Approche le robot du joueur j.
 * Passe par le chemin le plus court
 *
 * @param j un joueur
 */
void robot::approcheJoueur(const joueur&j) //déplace le robot vers le joueur passé en paramètre, en passant pas le plus court chemin
{
    if(x()>j.x())
    {
        if(y()>j.y())
        {
            if((x()-j.x())<(y()-j.y()))
            {
                deplaceHaut();
            }
            else
            {
                deplaceGauche();
            }
        }
        else
        {
            if((x()-j.x())<(j.y()-y()))
            {
                deplaceBas();
            }
            else
            {
                deplaceGauche();
            }
        }
    }
    else if (x()<j.x())
    {
        if(y()>j.y())
        {
            if((j.x()-x())<(y()-j.y()))
            {
                deplaceHaut();
            }
            else
            {
                deplaceDroite();
            }
        }
        else
        {
            if((j.x()-x())<(j.y()-y()))
            {
                deplaceBas();
            }
            else
            {
                deplaceDroite();
            }
        }
    }
    else{
        if(y()>j.y())
        {
            deplaceHaut();
        }
        else if(y()<j.y())
        {
            deplaceBas();
        }
        else
            return;
    }


}
