#include <iostream>
#include "graphics.h"
#include "terrain.h"
#include "robot.h"
#include "joueur.h"
#include "jeu.h"

int main()
{
    int nbRobots1;
    int nbRobots2;
    int tailleTerrain;
    std::cout<<"Bienvenue dans le jeu de robot ecraseur"<<std::endl<<"Combien de robot normaux voulez vous ?";
    std::cin>>nbRobots1;
    std::cout<<std::endl<<"Combien de robots ameliores voulez vous ?";
    std::cin>>nbRobots2;
    std::cout<<std::endl<<"De combien de case voulez vous que le terrain soit ?";
    std::cin>>tailleTerrain;
    int tailleAffiche=800;
    opengraphsize(tailleAffiche+20,tailleAffiche+20);
    terrain t(tailleTerrain);
    jeu j(nbRobots1,nbRobots2,t,tailleAffiche);
    j.lancer();
    closegraph();

    char reponse;
    std::cout<<"Souhaitez vous sauvegarder le terrain ? (y/n)";
    std::cin>>reponse;
    while(reponse!='y'&&reponse!='n')
    {
        std::cout<<"Souhaitez vous sauvegarder le terrain ? (y/n)";
        std::cin>>reponse;
    }
    if(reponse=='y')
    {
        t.sauver("fichierJeu.txt");
    }

    std::cout<<"BOOM BITCH";
}
